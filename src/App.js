import React, { Component } from "react";

import AppNavbar from "./components/AppNavbar";

import {BrowserRouter,Route} from 'react-router-dom';

import Home from './components/Home'
import Text2ISL from './components/Text2ISL'
import ISL2Text from './components/ISL2Text'

class App extends Component {
  render() {
    return (
      <div className="App">

        <BrowserRouter>
          <AppNavbar />
          <Route exact path="/"><Home/></Route>
         <Route path="/ISL2Text"><ISL2Text/></Route>
         <Route path="/Text2ISL"><Text2ISL/></Route>
        </BrowserRouter>

      </div>
    );
  }
}



export default App;
