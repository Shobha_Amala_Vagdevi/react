import React, { Component } from "react";
import { FaFilter,FaTags,FaLayerGroup } from 'react-icons/fa';// for Icons
import {Link} from 'react-router-dom'
import {
Row,
Col,
Button,
ButtonDropdown,
DropdownToggle,
DropdownMenu,
DropdownItem,
Navbar, NavbarBrand, Container} from "reactstrap";

class AppNavbar extends Component {


  render() {

    return (
      <div>
        <Navbar color="secondary" dark expand="sm">
          <Container fluid>
            <NavbarBrand href="/">
              <span>{"WinVinaya"}</span>
            </NavbarBrand>
          </Container>
          <Button href="/Text2ISL">Text2ISL</Button>
          <Button href="/ISL2Text">ISL2Text</Button>
        </Navbar>
      </div>
    );
  }
}

export default AppNavbar;
